# Kubernetes Demo

[![IaC](https://app.demo.soluble.cloud/api/v1/public/badges/c9eef2ca-e09d-4753-9f91-1ece34018e6e.svg?orgId=463027545225)](https://app.demo.soluble.cloud/repos/details/bitbucket.org/marionbit/kubernetes?orgId=463027545225)  [![CIS](https://app.demo.soluble.cloud/api/v1/public/badges/d296cecd-a7b3-4b88-9e78-bdd4b57b17e6.svg?orgId=463027545225)](https://app.demo.soluble.cloud/repos/details/bitbucket.org/marionbit/kubernetes?orgId=463027545225)  

[![CIS](https://app.demo.soluble.cloud/api/v1/public/badges/9e684111-ad36-43eb-acfe-13394cc9ea41.svg)](https://app.demo.soluble.cloud/repos/details/github.com/insecurecorp/kubernetes)  [![IaC](https://app.demo.soluble.cloud/api/v1/public/badges/ae93700f-fd82-4f76-b216-64c474373bf6.svg)](https://app.demo.soluble.cloud/repos/details/github.com/insecurecorp/kubernetes)  

![kubernetes](.images/sad-cloud.png)
